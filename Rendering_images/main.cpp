// this code is created by LetMe on 2016/12/11
// last updated on 2016/12/18
// review of this code: 

#define SDL_MAIN_HANDLED // handle intialization manually.
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <assert.h>


int main(int argc, char** argv)
{
	assert(argc == 3);
	// Start SDL
	SDL_Init(SDL_INIT_EVERYTHING);  
	IMG_Init(IMG_INIT_JPG);

	SDL_Event event;

	SDL_Window* main_win = NULL;
	SDL_Surface* s_bkgd = NULL;
	SDL_Texture* texture_bkgd = NULL;
	SDL_Surface* s_card = NULL;
	SDL_Texture* texture_card = NULL;


	SDL_Renderer* main_renderer = NULL;
	SDL_Rect pos_card;
	pos_card.w = 100;
	pos_card.h = 125;
	pos_card.x = 580;
	pos_card.y = 300;
	
	bool exec_flag = true;


    main_win = SDL_CreateWindow(
	                             "Example 01",
								 SDL_WINDOWPOS_CENTERED,
								 SDL_WINDOWPOS_CENTERED,
								 960,
								 512,
								 SDL_WINDOW_RESIZABLE
							   );
	
	main_renderer = SDL_CreateRenderer(main_win, -1, 
	                                   SDL_RENDERER_ACCELERATED | 
									   SDL_RENDERER_PRESENTVSYNC);
	// using hardware acceleration.

	s_bkgd = IMG_Load(argv[1]);
	texture_bkgd = SDL_CreateTextureFromSurface(main_renderer, s_bkgd);
	s_card = IMG_Load(argv[2]);
	texture_card = SDL_CreateTextureFromSurface(main_renderer, s_card);

	
	while(exec_flag)
	{
		SDL_PollEvent(&event);
		if(event.type == SDL_QUIT)	exec_flag = false;

		SDL_RenderClear(main_renderer);
		SDL_RenderCopy(main_renderer, texture_bkgd, NULL, NULL);
		SDL_RenderCopy(main_renderer, texture_card, NULL, &pos_card);
		SDL_RenderPresent(main_renderer);
	}
	
	
	SDL_DestroyWindow(main_win);
	SDL_FreeSurface(s_bkgd);
	SDL_DestroyTexture(texture_bkgd);
	SDL_FreeSurface(s_card);
	SDL_DestroyTexture(texture_card);

	SDL_DestroyRenderer(main_renderer);
	
	IMG_Quit();

	// Quit SDL
	SDL_Quit();
	
	return 0;
}
